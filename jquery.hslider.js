/* ===========================================================
 * jQuery Horizontal Slider.js v1
 * ===========================================================
 *
 *
 * All animation is powered in CSS3
 * Only for modern browsers
 *
 * ========================================================== */

(function($){

  var defaults = {
    easing:"ease",
    animationTime:1300
    //pagination:true
  };


  $.fn.HSlider = function(options){

    var settings = $.extend({}, defaults, options),
        $slider = $(this),
        sections = $("section"),
        total = sections.length,
        quiet = false,
        quietOnOut = false,
        disableScroll = false,
        timeout = Number(settings.animationTime) + 100; // make sure Number

    document.body.style.overflow = "hidden";
    var windowW = $(window).width();
    document.body.style.overflow = "";

    console.log(windowW);

    $.fn.transformPage = function(settings,pos){

      var $this = $(this);

      requestAnimationFrame(function () {

        $this.css({
          "-webkit-transform": "translate3d(" + pos + "px, 0 ,0)",
          "-webkit-transition": "all " + settings.animationTime + "ms " + settings.easing,
          "transform": "translate3d(" + pos + "px, 0 ,0)",
          "transition": "all " + settings.animationTime + "ms " + settings.easing
        });

      });

      return $(this);
    };

    $.fn.slideLeft = function(){
      var _index = $("section.active").data("index");
      if (_index < total) location.hash = '#'+ (_index + 1);

      return $(this);
    };

    $.fn.slideRight = function(){
      var _index = $("section.active").data("index");
      if (_index <= total && _index > 1) location.hash = '#'+ (_index - 1);

      return $(this);
    };

    // Seperate RENDER, support URL hash
    $.fn._render = function(){
      var _hash = Math.floor(Number(location.hash.split('#')[1]));	// get hash, do type cast
      _hash = _hash ?   _hash : 1;									// undefined, 0, NaN
      if(_hash < 1) 	  _hash = 1;									// prevent pre overflow
      if(_hash > total) _hash = total;								// prevent post overflow
      var _activeIndex = _hash;

      // reset current
      $("section.active").removeClass("active");
      $(".pagination li a" + ".active").removeClass("active");

      // activate new
      $("section[data-index=" + _activeIndex + "]").addClass("active");
      $(".pagination li a" + "[data-index=" + _activeIndex + "]").addClass("active");

      // calculate pos and transform
      var pos = ((_activeIndex - 1) * windowW) * -1;

      $slider.transformPage(settings, pos);

      return $(this);
    };

    $.fn._bindEvent = function(){

      var firstHash = $slider.find('.section').first().data('index'),
          lastHash = $slider.find('.section').last().data('index'),
          $firstSec = $(".section[data-index=" + firstHash + "]"),
          $lastSec = $(".section[data-index=" + lastHash + "]");

      function sFixed() {
        $slider.css({
          'position': 'fixed',
          'top': 0,
          'left': 0,
          'right': 0,
          'bottom': 0
        });

        $('body').css({
         'overflow-y': 'hidden'
        });
      }

      function sUnFixed() {
        $slider.css({
          'position': 'static'
        });

        $('body').css({
         'overflow-y': 'auto'
         });
      }

      //Reset window width
      $(window).on('resize', function(){
        windowW = $(this).width();
      });

      // HashChange
      $(window).on('hashchange', $slider._render);

      $(window).on('scroll', function() {
        var yWindowTop = window.scrollY,
            ySliderTop = $slider.offset().top,
            yWindowBot = yWindowTop + $(window).height(),
            ySliderBot = ySliderTop + $slider.height();

        if (yWindowTop >= ySliderTop && !$lastSec.hasClass('active')) {
          sFixed();
          quietOnOut = true;
        }

        if (yWindowBot <= ySliderBot && !$firstSec.hasClass('active')) {
          sFixed();
          quietOnOut = true;
        }

      });

      //Mousewheel MouseScroll
      $(document).on('mousewheel', function(event) {

        var delta = event.deltaY,
            deltaFactor = event.deltaFactor,
            timeOnOut = 500;

        if (quietOnOut) {

          if ((delta < 0 && !$lastSec.hasClass('active')) || (delta > 0 && !$firstSec.hasClass('active'))) {
            disableScroll = true;
          } else {
            //@TODO: fix after out slider
            //setTimeout(function(){
              sUnFixed();
              disableScroll = false;
            //}, timeout);

            quietOnOut = false;
          }

          if ($firstSec.hasClass('active') ||$lastSec.hasClass('active')) {
            setTimeout(function(){
              $slider._handleMouseScroll(event, delta);
            },timeOnOut)
          } else {
            $slider._handleMouseScroll(event, delta);
          }

        }

        if (disableScroll) {
          event.preventDefault();
        }

      });

      return $(this);
    };

    $.fn._handleMouseScroll = function(event, delta){
      if (quiet == false) {
        if (delta == 0) return; 	// early return
        if (delta < 0) {
          $slider.slideLeft();
        } else {
          $slider.slideRight();
        }

        quiet = true;

        // deal with OSX inertia scroll
        setTimeout(function(){
          quiet = false;
        }, timeout);
      }
    };

    $.fn._initStyle = function(){

      $slider.addClass("HSlider").css({
        "position":"relative",
        "width": total * windowW,
        "height":"100%"
      });

      $.each(sections,function(i){

        $(this).css({
          "width":windowW,
          "height":"100%"
        }).addClass("section").attr("data-index", i+1);


      });

      return $(this);
    };

    // create slider
    $slider
      ._initStyle()
      ._bindEvent()
      ._render();
  }

})(window.jQuery);
